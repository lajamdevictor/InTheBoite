using UnityEngine;


public class Balance : MonoBehaviour
{
    Rigidbody2D rb;

    public float targetRotation;
    public float force;

    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rb.MoveRotation(Mathf.LerpAngle(rb.rotation, targetRotation, force * Time.deltaTime));
    }
}